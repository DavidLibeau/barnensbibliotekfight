# BarnensBibliotekFight

- Axure prototype (simulated website): https://4bvrhm.axshare.com/barnensbibliotek.html
- Showcase UI: https://4bvrhm.axshare.com/game.html
- Standalone online version : http://dav.li/barnensbibliotekfight/ 
- Git : https://framagit.org/DavidLibeau/barnensbibliotekfight

## URL API

In `http://dav.li/barnensbibliotekfight/?1-2` the `1-2` corresponding to the fighter's ID, 1 vs 2. The first one is the the player's one, the second one is the opponent's one.


## XML API

### Cards

Add cards by adding `<card>` tags.

Example : `<card>Attack,2</card>`

You need to specify the card type (between `Attack`, `Defense`, `Heal` and `Clear`) and the card value (Note : the Clear card value is useless).
