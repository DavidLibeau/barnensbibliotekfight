class Fighter {
    constructor(id, owner, imgSelector, lifeSelector, dialogSelector) {
        this.id = id;
        this.element = $(gameData).find("fighter[id=\"" + this.id + "\"]>element")[0].innerHTML;
        this.name = $(gameData).find("fighter[id=\"" + this.id + "\"]>name")[0].innerHTML;
        this.maxLife = parseInt($(gameData).find("fighter[id=\"" + this.id + "\"]>life")[0].innerHTML);
        this.life = this.maxLife;
        this.lifeSelector = lifeSelector;
        this.dialogSelector = dialogSelector;
        this.dialogSpoken = {};
        this.owner = owner;
        this.selector = owner.selector;

        $(imgSelector).attr("src", "img/" + $(gameData).find("fighter[id=\"" + this.id + "\"]>img")[0].innerHTML);
    }
    decreaseLife(points) {
        console.log("--- DECREASE LIFE ---");
        this.life -= parseInt(points);

        var lifeDialogs = $(gameData).find("fighter[id=\"" + this.id + "\"]>dialog>life");
        console.log(lifeDialogs);
        for (var d = 0; d < lifeDialogs.length; d++) {
            console.log(this.life + "<=" + parseFloat($(lifeDialogs[d]).attr("factor")) * this.maxLife);
            console.log(lifeDialogs[d]);
            if (this.life <= parseFloat($(lifeDialogs[d]).attr("factor")) * this.maxLife) {
                if (this.dialogSpoken[$(lifeDialogs[d]).attr("factor")] == undefined) {
                    this.dialogSpoken[$(lifeDialogs[d]).attr("factor")] = true;
                    this.tempDialog = $(lifeDialogs[d]).innerHTML;
                } else {
                    console.log("Dialog (life factor=" + $(lifeDialogs[d]).attr("factor") + ") already spoken.");
                }
            }
        }
        if (this.tempDialog != undefined && this.tempDialog != "") {
            this.speak(this.tempDialog);
        }

        console.log(this.life);

        if (this.life <= 0) {
            this.life = 0;
            game.end();
        }
        this.refresh();
    }
    increaseLife(points) {
        this.life += points;
        if (this.life > this.maxLife) {
            this.life = this.maxLife;
        }
        this.refresh();
    }
    refresh() {
        $(this.lifeSelector).html(this.life);
    }
    speak(something) {
        console.log("\"" + something + "\"");
        $(this.dialogSelector).fadeIn().text(something);
        var selector = this.dialogSelector;
        var timeShown = something.length * 80;
        if (timeShown < 4000) {
            timeShown = 4000;
        }
        setTimeout(function () {
            console.log(selector);
            $(selector).fadeOut();
        }, timeShown);
    }
}