class Game {
    constructor(playerFighterId, opponentFighterId, level) {
        $("#menu,#newGame").fadeOut();
        console.log("Game is starting : " + playerFighterId + " vs " + opponentFighterId);
        var cardsPerTurn = $(gameData).find("fighter[id=\"" + playerFighterId + "\"]>cardsPerTurn")[0];
        if (cardsPerTurn != undefined) {
            cardsPerTurn = parseInt(cardsPerTurn.innerHTML);
        } else {
            cardsPerTurn = 1;
        }
        player = new Player(cardsPerTurn, playerFighterId, true, opponent, "#biblomon", "#hand", "#biblomonBoard", "#biblomonImg", "#biblomonLife", "#biblomonDialog");
        opponent = new Player(1, opponentFighterId, false, player, "#bokdrake", "#opponentHand", "#bokdrakeBoard", "#bokdrakeImg", "#bokdrakeLife", "#bokdrakeDialog");
        player.opponent = opponent;

        this.level = localStorage.getItem("level");
        if (this.level == undefined) {
            console.error("Error retrieving level from local storage");
            localStorage.setItem("level", 1);
            this.level = 1;
        }

        this.turnsPlayed = 0;
        this.ended = false;

        this.playerPlaying = player;

        for (var c in player.deck.cards) {
            $("#tabdeck").append(player.deck.cards[c].display("",true));
        }

        player.hand.drawMissingCards();
        opponent.hand.drawMissingCards();

        addToTimeline(0,this.refresh);

        player.fighter.speak($(gameData).find("fighter[id=\"" + player.fighter.id + "\"]>dialog>start")[0].innerHTML);
    }
    refresh() {
        player.refresh();
        opponent.refresh();
    }
    decreaseLifetimeOfBoard(board) {
        var allCardsOnBoard = $(board.selector + ">.card");
        allCardsOnBoard.each(function () {
            var cardId = $(this).attr("id");
            cardId = parseInt(cardId.replace("card-", ""));
            var card = allCards[cardId];
            card.decreaseLifetime();
        });
    }
    nextTurn() {
        console.log("-- Next move --");
        if (game.playerPlaying.cardsOnHold.length >= game.playerPlaying.cardsPerTurn) {
            console.log("----- Turn ended -----");
            game.turnsPlayed++;
            console.log("Cards on hold :");
            console.log(game.playerPlaying.cardsOnHold);
            for (var c in game.playerPlaying.cardsOnHold) {
                game.playerPlaying.cardsOnHold[c].play();
            }
            game.playerPlaying.cardsOnHold = [];

            addToTimeline(200, function () {
                $(".card.played").fadeOut(200);
                player.board.refresh();
                opponent.board.refresh();
            });

            $(player.hand.selector).addClass("disabled");
            if (game.playerPlaying == opponent) {
                game.refresh();
                opponent.hand.drawMissingCards();
                $(player.hand.selector).removeClass("disabled");
                player.hand.drawMissingCards();
                console.log("new");
                //$(".card.new").removeClass("new");
            }

            if (!game.ended) {

                game.playerPlaying.setCanPlay(false);
                game.playerPlaying.opponent.setCanPlay(true);

                if (game.playerPlaying == player) {
                    addToTimeline(300, function () {
                        $("#opponentHand").animate({
                            top: "-200px"
                        }, 300);
                    });
                    addToTimeline(1000 + Math.floor(Math.random() * 4000), function () {
                        game.decreaseLifetimeOfBoard(game.playerPlaying.board);
                        game.playerPlaying.opponent.playTurn();
                        game.playerPlaying = game.playerPlaying.opponent;
                    });
                    addToTimeline(300, function () {
                        $("#opponentHand").animate({
                            top: "-170px"
                        }, 300);
                    });
                } else {
                    addToTimeline(0, function () {
                        game.decreaseLifetimeOfBoard(game.playerPlaying.board);
                        game.playerPlaying.opponent.playTurn();
                        game.playerPlaying = game.playerPlaying.opponent;
                    });
                }
            } else {
                player.setCanPlay(false);
                opponent.setCanPlay(false);
            }
        }

    }
    end() {
        game.ended = true;
        player.setCanPlay(false);
        opponent.setCanPlay(false);

        player.hand.removeAllCards();
        opponent.hand.removeAllCards();

        player.board.clear();
        opponent.board.clear();

        game.refresh();

        $("#biblomon,#biblomonDialog,#bokdrake,#bokdrakeDialog").css("z-index", 100);

        console.log("GAME END");
        addToTimeline(0, function () {
            if (player.fighter.life > 0) {
                localStorage.setItem("level", parseInt(localStorage.getItem("level")) + 1);
                var gameLevel = parseInt(localStorage.getItem("level"));

                if ((gameLevel - 1) < 8) {
                    $("#endGame").html('<h2>Du vann!</h2><p>Grattis! Du klarade nivå ' + (gameLevel - 1) + '!</p><h2>Du låste upp:</h2><ul></ul><div class="buttons"><a href="?' + new Date().getMilliseconds() + '">Spela nivå ' + gameLevel + '</a></div>');
                } else {
                    $("#endGame").html('<h2>Du vann!</h2><p>Grattis! Du klarade nivå ' + (gameLevel - 1) + ' och du besegrade alla Bokdrakar! Du klarade spelet!</p>');
                }

                $(gameData).find("fighter").each(function () {
                    var type = $(this).find("type")[0].innerHTML;
                    var level = $(this).find("level")[0].innerHTML;
                    if (type == "biblomon" && level != "") {
                        if (level == gameLevel) {
                            var id = $(this).attr("id");
                            var name = $(this).find("name")[0].innerHTML;
                            var imgSrc = "img/" + $(this).find("img")[0].innerHTML;
                            var element = $(this).find("element")[0].innerHTML;
                            var handSize = $(this).find("handSize")[0].innerHTML;
                            var cardsPerTurn = $(this).find("cardsPerTurn")[0].innerHTML;
                            var life = $(this).find("life")[0].innerHTML;
                            $("#endGame>ul").append("<li><img src=\"" + imgSrc + "\" alt=\"Img\"/><h3>" + name + "</h3><ul><li>Element : " + element + "</li><li>Life : " + life + "</li><li>Hand size : " + handSize + " cards</li><li>Card" + [cardsPerTurn > 1 ? "s" : ""] + " per turn : " + cardsPerTurn + "</li></ul></li>");
                        }
                    }
                });

                player.fighter.speak($(gameData).find("fighter[id=\"" + player.fighter.id + "\"]>dialog>victory")[0].innerHTML);
                setTimeout(function () {
                    opponent.fighter.speak($(gameData).find("fighter[id=\"" + opponent.fighter.id + "\"]>dialog>defeat")[0].innerHTML);
                }, 5000);
            } else {
                $("#endGame").html('<h2>Du förlorade... :(</h2><p>Du blev besegrad av bokdraken!</p><div class="buttons"><a href="?' + new Date().getMilliseconds() + '">Försök igen</a></div>');
                //<a href="?' + player.fighter.id + '-' + opponent.fighter.id + '">Try again</a>

                setTimeout(function () {
                    player.fighter.speak($(gameData).find("fighter[id=\"" + player.fighter.id + "\"]>dialog>defeat")[0].innerHTML);
                }, 5000);
                opponent.fighter.speak($(gameData).find("fighter[id=\"" + opponent.fighter.id + "\"]>dialog>victory")[0].innerHTML);
            }
            $("#endGame").fadeIn();
        });
    }

}