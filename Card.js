class Card {
    constructor(type, value, temp = false) {
        this.temp = temp;
        if (!this.temp) {
            do {
                var tempId = Math.floor(Math.random() * 1000);
            } while (typeof allCards[tempId] != "undefined");
            allCards[tempId] = this;
            this.id = tempId;
            this.selector = "#card-" + this.id;
        }
        this.type = type; // "Attack", "Sköld", "Hälsokort", "Renskort"
        this.value = value; // int
        this.bonus = 0;
        this.valueUsed = 0;
        this.lifetime = 3;
    }
    duplicate() {
        var tempCard = new Card(this.type, this.value, true);
        tempCard.id = this.id;
        tempCard.selector = "#card-" + this.id + "-temp";
        return tempCard;
    }
    ownedBy(hand) {
        console.log("Card #" + this.id + " Type: " + this.type + " Value: " + this.value + " Lifetime: " + this.lifetime + " ownedBy:");
        console.log(hand);
        this.hand = hand;
    }
    removeOwnership() {
        this.hand = undefined;
        this.bonus = 0;
    }
    decreaseLifetime() {
        this.lifetime--;
        if (this.lifetime <= 0) {
            this.hand.owner.board.removeCard(this);
            this.removeOwnership();
        }
    }
    log() {
        console.log("[Card] \nType: " + this.type + " \nValue: " + this.value + " \nLifetime: " + this.lifetime);
    }
    display(addedClass = "", inDeck = false) {
        if (this.type == "Sköld") {
            return ("<span " + [inDeck ? "data-" : ""] + "id=\"card-" + this.id + [this.temp ? "-temp" : ""] + "\" class=\"card " + addedClass + "\"><span class=\"lifetime\">" + this.lifetime + "</span><span class=\"value\">" + this.value + "</span><span class=\"type\"><img src=\"img/Defense.svg\" alt=\"" + this.type + "\"/><p>" + this.type + "</p></span></span>");
        } else if (this.type == "Renskort") {
            return ("<span " + [inDeck ? "data-" : ""] + "id=\"card-" + this.id + [this.temp ? "-temp" : ""] + "\" class=\"card " + addedClass + "\"><span class=\"lifetime\"><i class=\"fa fa-bolt\" aria-hidden=\"true\"></i></span><span class=\"value\"></span><span class=\"type\"><img src=\"img/Clear.svg\" alt=\"" + this.type + "\"/><p>" + this.type + "</p></span></span>");
        } else if (this.type == "Attack") {
            console.log(this.bonus);
            return ("<span " + [inDeck ? "data-" : ""] + "id=\"card-" + this.id + [this.temp ? "-temp" : ""] + "\" class=\"card " + addedClass + "\"><span class=\"lifetime\"><i class=\"fa fa-bolt\" aria-hidden=\"true\"></i></span><span class=\"value\">" + this.value + "</span><span class=\"type\"><img src=\"img/Attack.svg\" alt=\"" + this.type + "\"/>" + [this.bonus != 0 ? "<span class=\"bonus\">+" + this.bonus + " bonus</span>" : ""] + "<p>" + this.type + "</p></span></span>");
        } else {
            return ("<span " + [inDeck ? "data-" : ""] + "id=\"card-" + this.id + [this.temp ? "-temp" : ""] + "\" class=\"card " + addedClass + "\"><span class=\"lifetime\"><i class=\"fa fa-bolt\" aria-hidden=\"true\"></i></span><span class=\"value\">" + this.value + "</span><span class=\"type\"><img src=\"img/Heal.svg\" alt=\"" + this.type + "\"/><p>" + this.type + "</p></span></span>");
        }
    }
    play() {
        this.hand.play(this);
    }
    animAttack(target, attackValue = 0) {
        var card = this;
        if (target == this.hand.owner.opponent.fighter && Math.random() < parseFloat($(gameData).find("fighter[id=\"" + this.hand.owner.opponent.fighter.id + "\"]>dodgeChance")[0].innerHTML)) { //Dodge
            if (card.hand != undefined && card.hand.owner == opponent && $(card.selector).length == 0) {
                card.opponentShowCard();
            }
            addToTimeline(500, function () {
                card.hand.owner.opponent.fighter.speak("Du missade!");
                $(card.hand.owner.opponent.fighter.selector).effect("shake", "left", 500, 1);
                $("#history").prepend("<li><strong>" + card.hand.owner.opponent.fighter.name + " undvek träffen!</strong></li>");
            });
            addToTimeline(0, function () {
                $("#card-" + card.id).remove();
            });
        } else {
            var randomHitId = Math.round(Math.random() * 100);
            if (target == this.hand.owner.opponent.fighter) {
                target.decreaseLife(card.value + card.bonus - card.valueUsed);
                addToTimeline(10, function () {
                    $(target.selector).append("<span id=\"hitPoints-" + randomHitId + "\" class=\"hitPoints\">" + (card.value + card.bonus - card.valueUsed) + "</span>");
                });
            } else {
                if (attackValue != 0) {
                    addToTimeline(10, function () {
                        $(target.selector).append("<span id=\"hitPoints-" + randomHitId + "\" class=\"hitPoints\">" + attackValue + "</span>");
                    });
                } else {
                    addToTimeline(10, function () {
                        $(target.selector).append("<span id=\"hitPoints-" + randomHitId + "\" class=\"hitPoints\">" + (card.value + card.bonus - card.valueUsed) + "</span>");
                    });
                }
            }
            this.moveToTarget(target, "easeInCubic", "easeOutCubic");

            addToTimeline(300, function () {
                console.log("scale:#hitPoints-" + randomHitId);
                $("#hitPoints-" + randomHitId).addClass("animate");
            });
            addToTimeline(0, function () {
                $("#hitPoints-" + randomHitId).fadeOut();
            });
            addToTimeline(0, function () {
                $("#hitPoints-" + randomHitId).remove();
            });

            if (target == this.hand.owner.opponent.fighter && Math.random() < parseFloat($(gameData).find("fighter[id=\"" + this.hand.owner.fighter.id + "\"]>critChance")[0].innerHTML)) { //Critical hit
                var randomCritId = Math.round(Math.random() * 100);
                addToTimeline(0, function () {
                    card.hand.owner.fighter.speak("Fullträff!");
                    $(target.selector).append("<span id=\"hitPoints-critical-" + randomCritId + "\" class=\"hitPoints critical\">" + Math.round(card.value * 0.5) + "</span>");
                    $("#history").prepend("<li><strong>" + card.hand.owner.opponent.fighter.name + " did a critical hit (+" + Math.round(card.value * 0.5) + ")!</strong></li>");
                });
                addToTimeline(300, function () {
                    console.log("scale:#hitPoints-critical-" + randomCritId);
                    $("#hitPoints-critical-" + randomCritId).addClass("animate");
                });
                addToTimeline(0, function () {
                    $("#hitPoints-critical-" + randomCritId).fadeOut();
                });
                addToTimeline(0, function () {
                    console.log("--");
                    $("#hitPoints-critical-" + randomCritId).remove();
                });
            }
        }
    }
    animHeal() {
        this.moveToTarget(this.hand.owner.fighter, "swing", "swing");
    }
    moveToTarget(target, easingIn = "swing", easingOut = "swing") {
        var card = this;
        if (card.hand != undefined && card.hand.owner == opponent && $(card.selector).length == 0) {
            card.opponentShowCard();
        }
        addToTimeline(500, function () {
            console.log("Begin anim : " + card.selector + " to target : " + target.selector);

            var $card = $(card.selector);
            if ($card.offset() && $(target.selector).offset()) {
                $card.attr("data-absolute-top", $card.offset().top);
                $card.attr("data-absolute-left", $card.offset().left + $card.width() / 2);
                $card.attr("data-relative-top", $card.css("top"));
                $card.attr("data-relative-left", $card.css("left"));

                $card.addClass("animated");
                $card.css({
                    "position": "fixed",
                    "top": $card.attr("data-absolute-top") + "px",
                    "left": $card.attr("data-absolute-left") + "px",
                });
                console.log($(target.selector).offset().top + "+" + $(target.selector).height() + "/2");
                $card.animate({
                    "top": $(target.selector).offset().top, //+$(target.selector).height()/2,
                    "left": $(target.selector).offset().left //+$(target.selector).width()/2
                }, 500, easingIn, function () {
                    console.log("Completed anim : " + card.selector);
                });
            }
        });
        addToTimeline(100, function () {
            var $card = $("#card-" + card.id);
            $card.animate({
                "top": $card.attr("data-absolute-top"),
                "left": $card.attr("data-absolute-left")
            }, 800, easingOut, function () {
                console.log("Completed anim");
                $card.removeClass("animated");
                if (card.hand != undefined && card.hand.owner == opponent) {
                    addToTimeline(0, function () {
                        $card.remove();
                    });
                } else {
                    $card.css({
                        "position": "relative",
                        "top": $card.attr("data-relative-top"),
                        "left": $card.attr("data-relative-left"),
                    });
                }
            });
        });
    }
    animClear(target) {
        var card = this;
        if (card.hand != undefined && card.hand.owner == opponent && $(card.selector).length == 0) {
            card.opponentShowCard();
        }
        addToTimeline(500, function () {
            console.log("Begin anim : " + card.selector + " to target : " + target.selector);
            var $card = $(card.selector);
            if ($card.offset() && $(target.selector).offset()) {

                $card.attr("data-absolute-top", $card.offset().top);
                $card.attr("data-absolute-left", $card.offset().left);
                $card.attr("data-relative-top", $card.css("top"));
                $card.attr("data-relative-left", $card.css("left"));

                $card.addClass("animated");
                $card.css({
                    "position": "fixed",
                    "top": $card.attr("data-absolute-top") + "px",
                    "left": $card.attr("data-absolute-left") + "px",
                });
                $card.animate({
                    "top": $(target.selector).offset().top + $(target.selector).height() / 2,
                    "left": $(target.selector).offset().left + $(target.selector).width() / 2
                }, 500, "swing", function () {
                    console.log("Completed anim : " + card.selector);
                });
            }
        });
        addToTimeline(200, function () {
            var $card = $(card.selector);
            $card.effect("shake", "left", 200, 3);
        });
        addToTimeline(800, function () {
            var $card = $("#card-" + card.id);
            $card.animate({
                "top": $card.attr("data-absolute-top"),
                "left": $card.attr("data-absolute-left")
            }, 800, "swing", function () {
                console.log("Completed anim");
                $card.removeClass("animated");
                if (card.hand != undefined && card.hand.owner == opponent) {
                    addToTimeline(0, function () {
                        $card.remove();
                    });
                } else {
                    $card.css({
                        "position": "relative",
                        "top": $card.attr("data-relative-top"),
                        "left": $card.attr("data-relative-left"),
                    });
                }
            });
        });
    }
    moveToRelativePosition(top, left, easingIn = "swing", easingOut = "swing") {
        var card = this;
        addToTimeline(500, function () {
            console.log("Begin anim");
            var $card = $(card.selector);

            $card.addClass("animated");
            console.log(top + " " + left);
            $card.animate({
                "top": parseInt($card.css("top")) + top + "px",
                "left": parseInt($card.css("left")) + left + "px",
            }, 500, easingIn, function () {
                console.log("Completed anim");
            });
        });
    }
    opponentShowCard() {
        var card = this;

        $("body").append(card.display());
        $("#card-" + this.id).css({
            "position": "fixed",
            "top": "-200px",
            "left": window.innerWidth / 2 //(Math.random()*(window.innerWidth/2))+window.innerWidth/4
        });

        addToTimeline(1000, function () {
            console.log("Begin anim");
            var $card = $("#card-" + card.id);

            $card.addClass("animated");
            $card.animate({
                "top": "30px",
            }, 300, "swing", function () {
                console.log("Completed anim");
            });
        });
        console.log(timeline);
        console.log($("#card-" + this.id).css("left"));
    }
}


function onPlayCard() { //when card is clicked or dropped (event "play")
    $(this).addClass("onHold");
    var cardId = $(this).attr("id");
    cardId = parseInt(cardId.replace("card-", ""));
    console.log("OnHold : " + cardId);
    var card = allCards[cardId];
    card.hand.owner.cardsOnHold.push(card);
    addToTimeline(100, game.nextTurn);
}