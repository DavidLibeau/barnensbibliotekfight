class Player {
    constructor(cardsPerTurn, fighterId, displayHand, opponentPlayer, selector, handSelector, boardSelector, imgSelector, lifeSelector, dialogSelector) {
        this.selector=selector;
        this.cardsPerTurn = cardsPerTurn;
        this.fighterId = fighterId;
        this.fighter = new Fighter(fighterId, this, imgSelector, lifeSelector, dialogSelector);
        this.deck = new Deck(this);
        this.opponent = opponentPlayer;
        this.hand = new Hand(this, handSelector);
        this.displayHand = displayHand;
        this.board = new Board(boardSelector);
        this.canPlay = true;
        this.cardsOnHold=[]
        this.refresh();
    }
    refresh() {
        if(this==player){
            console.log("refresh");
        }
        this.hand.refresh();
        this.board.refresh();
        this.fighter.refresh();
        if (this.deck.cards.length <= 3) {
            console.log("--- Renew deck ---");
            this.deck = new Deck(this);
        }
    }
    playTurn() {
        if (this == opponent) {
            this.hand.playRandomCard();
        }
    }
    setCanPlay(value) {
        this.canPlay = value;
    }
}