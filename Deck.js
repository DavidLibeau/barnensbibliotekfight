class Deck {
    constructor(owner) {
        this.owner = owner;
        this.cards = new Array();
        var cardsData = $(gameData).find("fighter[id=\"" + this.owner.fighterId + "\"]>deck>card");
        console.log(cardsData);
        for (var c = 0; c < cardsData.length; c++) {
            if (c != "length" && c != "prevObject" && c != "jquery" && c != "constructor") {
                var cardData = cardsData[c].innerHTML.split(",");
                this.cards.push(new Card(cardData[0], parseInt(cardData[1])));
            }
        }
        if (this.cards.length == 0) {
            console.log("Error while retrieving cards from XML, using default Deck values.");
            this.cards = [
                new Card("Attack", 2),
                new Card("Attack", 2),
                new Card("Attack", 2),
                new Card("Attack", 2),
                new Card("Attack", 4),
                new Card("Attack", 4),
                new Card("Attack", 4),
                new Card("Attack", 6),
                new Card("Sköld", 2),
                new Card("Sköld", 2),
                new Card("Sköld", 2),
                new Card("Sköld", 3),
                new Card("Sköld", 3),
                new Card("Sköld", 3),
                new Card("Sköld", 4),
                new Card("Sköld", 4),
                new Card("Hälsokort", 1),
                new Card("Hälsokort", 1),
                new Card("Hälsokort", 1),
                new Card("Hälsokort", 2),
                new Card("Hälsokort", 2),
                new Card("Hälsokort", 3),
                new Card("Renskort", 0),
            ];
        }
    }
    draw() {
        var c = Math.floor(Math.random() * this.cards.length);
        var card = this.cards[c];
        this.cards.splice(c, 1);
        return (card);
    }
}