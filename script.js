"use strict";
$(document).ready(init);

var game;
var player;
var opponent;
var gameData;

var allCards = [];
var timeline = [];

function init() {
    $("#startScreen").click(function(){
        $(this).fadeOut();
    });
    
    var gameLevel = localStorage.getItem("level");
    if (gameLevel == undefined) {
        console.log("Level not defined in local storage, defining 1");
        gameLevel = 1;
        localStorage.setItem("level", gameLevel);
    }
    
    if(gameLevel<8){
        $("#newGame>h2").text("Nivå "+gameLevel);
    }
    
    tickTimeline(); //Launch timeline loop

    $("#menu").tabs();
    $(".toggleMenu").click(function () {
        $("#menu").toggle();
        if ($(this).attr("href")) {
            console.log($(this).attr("href"));
            $("#menu").tabs("option", "active", $(this).attr("href"));
        }
    });
    $("#tabgame").html('<p>Du kan avsluta den här nivån och försöka igen.</p><div class="buttons"><a href="?' + new Date().getMilliseconds() + '">Nytt spel</a></div>');

    if (window.location.href.indexOf("-") != -1) {
        var fightersParams = window.location.href.split("?");
        if (fightersParams[1] != undefined) {
            fightersParams = fightersParams[1].split("-");
        } else {
            fightersParams = undefined;
        }
        console.log(fightersParams);
    } else {
        console.log("\"-\" not found");
        fightersParams = undefined;
    }

    $.ajax("./gameData.xml", {
        dataType: "xml"
    }).done(function (data) {
        gameData = data;

        //Choose opponent
        var allOpponents = [];
        $(gameData).find("fighter").each(function () {
            var type = $(this).find("type")[0].innerHTML;
            var level = $(this).find("level")[0].innerHTML;
            if (type == "bokdrake" && (level == gameLevel || gameLevel>8)) {
                allOpponents.push($(this));
            }
        });
        console.log(allOpponents);
        var choosenOpponent = allOpponents[Math.floor(Math.random() * allOpponents.length)];
        var choosenOpponentId = choosenOpponent.attr("id");
        var choosenOpponentName = choosenOpponent.find("name")[0].innerHTML;
        var choosenOpponentImgSrc = "img/" + choosenOpponent.find("img")[0].innerHTML;
        var choosenOpponentElement = choosenOpponent.find("element")[0].innerHTML;
        var choosenOpponentHandSize = choosenOpponent.find("handSize")[0].innerHTML;
        var choosenOpponentCardsPerTurn = choosenOpponent.find("cardsPerTurn")[0].innerHTML;
        var choosenOpponentLife = choosenOpponent.find("life")[0].innerHTML;
        var choosenOpponentStartDialog = choosenOpponent.find("dialog>start")[0];
        if (choosenOpponentStartDialog == undefined) {
            choosenOpponentStartDialog = "I will fight you!"
        } else {
            choosenOpponentStartDialog = choosenOpponentStartDialog.innerHTML;
        }
        console.log(choosenOpponentStartDialog);
        $("#bokdrakeOpponent").html("<li><img src=\"" + choosenOpponentImgSrc + "\" alt=\"Img\"/><h3>" + choosenOpponentName + "</h3><ul><li>Element : " + choosenOpponentElement + "</li><li>Hälsopoäng : " + choosenOpponentLife + "</li><li>Antal kort på hand : " + choosenOpponentHandSize + " cards</li><li>Kort spelade per runda : " + choosenOpponentCardsPerTurn + "</li></ul></li><li class=\"dialog\">" + choosenOpponentStartDialog + "</li>");

        //List Biblomon
        $(gameData).find("fighter").each(function () {
            var type = $(this).find("type")[0].innerHTML;
            var level = $(this).find("level")[0].innerHTML;
            if (type == "biblomon" && level!="") {
                if (level <= gameLevel) {
                    var id = $(this).attr("id");
                    var name = $(this).find("name")[0].innerHTML;
                    var imgSrc = "img/" + $(this).find("img")[0].innerHTML;
                    var element = $(this).find("element")[0].innerHTML;
                    var handSize = $(this).find("handSize")[0].innerHTML;
                    var cardsPerTurn = $(this).find("cardsPerTurn")[0].innerHTML;
                    var life = $(this).find("life")[0].innerHTML;
                    $("#biblomonList").append("<li><img src=\"" + imgSrc + "\" alt=\"Img\"/><h3>" + name + "</h3><ul><li>Element : " + element + "</li><li>Hälsopoäng : " + life + "</li><li>Antal kort på hand : " + handSize + " cards</li><li>Kort spelade per runda : " + cardsPerTurn + "</li></ul><button class=\"choose\" data-fighterId=\"" + id + "\">Välj den här kämpen</button></li>");
                }
            }
        });
        $(gameData).find("fighter").each(function () {
            var type = $(this).find("type")[0].innerHTML;
            var level = $(this).find("level")[0].innerHTML;
            if (type == "biblomon" && level!="") {
                if (level > gameLevel) {
                    var id = $(this).attr("id");
                    var name = $(this).find("name")[0].innerHTML;
                    var imgSrc = "img/" + $(this).find("img")[0].innerHTML;
                    var element = $(this).find("element")[0].innerHTML;
                    var handSize = $(this).find("handSize")[0].innerHTML;
                    var life = $(this).find("life")[0].innerHTML;
                    $("#biblomonList").append("<li class=\"locked\" title=\"Locked Biblomon\"><img src=\"img/lockedBiblomon.svg\" alt=\"Img\"/><h3>" + name + "</h3></li>");
                }
            }
        });
        $("button.choose").click(function () {
            game = new Game($(this).attr("data-fighterId"), choosenOpponentId, gameLevel);
        });

        //Start game
        if (fightersParams != undefined) {
            game = new Game(parseInt(fightersParams[0]), parseInt(fightersParams[1]));
        }

    });

    // Drag ANIMATION
    setInterval(function () {
        $("#hand>.card.ui-draggable-dragging:not(.animated), #hand>.card.played:not(.animated)").each(function () {
            var boardBottom = $("#biblomonBoard").offset().top + $("#biblomonBoard").height();
            var boardTop = $("#biblomonBoard").offset().top;
            var boardMiddle = $("#biblomonBoard").offset().top + ($("#biblomonBoard").height() / 2);
            var boardHeight = $("#biblomonBoard").height();
            var cardMiddle = $(this).offset().top + ($(this).height() / 2);
            var onBoardModifier = 0
            if (boardBottom > cardMiddle && boardTop < cardMiddle) {
                onBoardModifier = 1 - Math.abs((cardMiddle - boardMiddle) / (boardHeight / 2));
            }
            var scale = 1;
            if (parseFloat($(this).css("top")) < 0) {
                var x = -0.001 * parseFloat($(this).css("top"));
            } else {
                var x = 0.001 * parseFloat($(this).css("top"));
            }

            if (parseFloat($(this).css("right")) < 0) {
                var y = -0.001 * parseFloat($(this).css("right"));
            } else {
                var y = 0.001 * parseFloat($(this).css("right"));
            }

            x = x * 3;
            //y=y*3;

            if (x == undefined || y == undefined) {
                scale = 1;
            } else {
                scale += x + y - (onBoardModifier * (x + y));
                if (scale < 1) { //for security
                    scale = 1;
                }
                if (scale > 1.3) {
                    scale = 1.3;
                }
            }

            var cardRight = parseFloat($(this).css("right"));
            var cardTop = parseFloat($(this).css("top"));
            if (cardTop > boardTop) {
                cardTop = boardTop;
            }
            var rightShadow = 0.1 * (cardRight - (onBoardModifier * cardRight));
            var topShadow = -0.1 * (cardTop - (onBoardModifier * cardTop));

            $(this).css({
                "box-shadow": rightShadow + "px " + topShadow + "px 20px 0px rgba(0,0,0,0.2)",
                "transform": "scale(" + scale + ")"
            });
        });
    }, 50);
}


function addToTimeline(duration, handler) {
    timeline.push([duration, handler]);
};

function tickTimeline() {
    /*console.log("- Tick -");
    console.log(timeline);*/
    if (timeline.length == 0) {
        setTimeout(function () {
            tickTimeline()
        }, 100);
    } else {
        console.log(timeline);
        timeline[0][1]();
        setTimeout(function () {
            tickTimeline()
        }, timeline[0][0]);
        timeline.splice(0, 1);
    }
};