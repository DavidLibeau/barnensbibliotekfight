class Board {
    constructor(selector) {
        this.cards = [];
        this.selector = selector;
    }
    clear() {
        this.cards = [];
    }
    removeCard(card) {
        for (var c in this.cards) {
            if (this.cards[c] == card) {
                this.cards.splice(c, 1);
            }
        }
    }
    add(card) {
        this.cards.push(card);
    }
    refresh(){
        $(this.selector).html(this.display());
    }
    display() {
        this.log();
        var htmlOutput = "";
        for (var c in this.cards) {
            htmlOutput += this.cards[c].display();
        }
        return(htmlOutput);
        //$(".card").on("click",onClickCardOnBoard);
    }
    log() {
        console.log("-- Board log --");
        var htmlOutput = "";
        for (var c in this.cards) {
            htmlOutput += this.cards[c].log();
        }
        console.log("/- Board log --");
    }
}