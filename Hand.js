class Hand {
    constructor(owner, selector) {
        this.owner = owner;
        this.selector = selector;
        if ($(gameData).find("fighter[id=\"" + this.owner.fighterId + "\"]>handSize")[0] == undefined) {
            this.size = 3;
        } else {
            this.size = parseInt($(gameData).find("fighter[id=\"" + this.owner.fighterId + "\"]>handSize")[0].innerHTML);;
        }
        this.cards = [];
    }
    play(card) {
        if (!game.ended) {
            if (this.owner.canPlay) {
                console.log("Play : " + card.id);
                $(card.selector).removeClass("onHold").addClass("played");
                this.removeCard(card);

                this.owner.fighter.tempDialog = "";
                this.owner.fighter.tempHistory = "";

                switch (card.type) {
                    case "Attack":
                        this.owner.fighter.tempHistory += this.owner.fighter.name + " spelar ett attackkort med ett värde av " + card.value + [card.bonus != 0 ? " (+" + card.bonus + " bonus) " : " "];

                        var totalCardValue = card.value + card.bonus;
                        //Apply attack
                        if (this.owner.opponent.board.cards.length == 0) { // 
                            this.owner.fighter.tempHistory += "som träffade din kämpe direkt eftersom du inte har någon sköld";
                            card.animAttack(this.owner.opponent.fighter);
                        } else if (this.owner.opponent.board.cards.length == 1) { //one defense
                            var attackValue = Math.round(card.value + card.bonus - card.valueUsed);
                            if (attackValue > this.owner.opponent.board.cards[0].value) { //need to destroy the defense then hit
                                this.owner.fighter.tempHistory += "som delvis blockerades av din sköld";
                                card.animAttack(this.owner.opponent.board.cards[0]);
                                card.valueUsed += Math.round(this.owner.opponent.board.cards[0].value);
                                this.owner.opponent.board.removeCard(this.owner.opponent.board.cards[0]);
                                card.animAttack(this.owner.opponent.fighter, attackValue);
                            } else if ((card.value + card.bonus - card.valueUsed) == this.owner.opponent.board.cards[0].value) { //can only hit the defense (and destroy it)
                                this.owner.fighter.tempHistory += "som helt blockerades av din sköld. Skölden är nu trasig!";
                                card.animAttack(this.owner.opponent.board.cards[0], attackValue);
                                card.valueUsed += Math.round(this.owner.opponent.board.cards[0].value);
                                this.owner.opponent.board.removeCard(this.owner.opponent.board.cards[0]);
                            } else { //can only hit the defense 
                                this.owner.fighter.tempHistory += "som helt blockerades av din sköld";
                                card.valueUsed += Math.round(this.owner.opponent.board.cards[0].value);
                                card.animAttack(this.owner.opponent.board.cards[0], attackValue);
                                this.owner.opponent.board.cards[0].value -= attackValue;
                            }
                        } else {
                            do {
                                console.log("this.owner.opponent.board.cards.length : " + this.owner.opponent.board.cards.length);
                                if (this.owner.opponent.board.cards.length == 0) {
                                    card.valueUsed = card.value + card.bonus;
                                    card.animAttack(this.owner.opponent.fighter);
                                } else {
                                    var cardSelected = undefined;
                                    for (var c in this.owner.opponent.board.cards) {
                                        if (cardSelected == undefined) {
                                            cardSelected = this.owner.opponent.board.cards[c];
                                        } else if (cardSelected.lifetime < this.owner.opponent.board.cards[c].lifetime) {
                                            cardSelected = this.owner.opponent.board.cards[c];
                                        }
                                    }
                                    console.log("Card selected :");
                                    cardSelected.log();

                                    if ((card.value + card.bonus - card.valueUsed) >= cardSelected.value) {
                                        card.animAttack(cardSelected, Math.round(card.value + card.bonus - card.valueUsed));
                                        this.owner.opponent.board.removeCard(cardSelected);
                                        card.valueUsed += Math.round(cardSelected.value);
                                    } else {
                                        var attackValue = Math.round(card.value + card.bonus - card.valueUsed);
                                        card.animAttack(cardSelected, attackValue);
                                        cardSelected.value -= attackValue;
                                        card.valueUsed += attackValue;
                                    }
                                }
                            } while ((card.value + card.bonus - card.valueUsed) > 0)

                        }
                        break;
                    case "Sköld":
                        this.owner.fighter.tempHistory += this.owner.fighter.name + " spelar ett sköldkort med ett värde av (" + card.value + ") ";
                        this.owner.board.add(card);
                        break;
                    case "Hälsokort":
                        this.owner.fighter.tempHistory += this.owner.fighter.name + " spelar ett hälsokort som återställer (" + card.value + ") poäng";
                        this.owner.fighter.increaseLife(card.value);

                        card.animHeal();
                        break;
                    case "Renskort":
                        this.owner.fighter.tempHistory += this.owner.fighter.name + " spelar ett kort som rensar motståndarens bräde";
                        this.owner.opponent.board.clear();

                        card.animClear(this.owner.opponent.board);
                        break;
                }

                console.log("> " + this.owner.fighter.tempHistory)
                $("#history").prepend("<li><strong>" + new Date().getHours() + ":" + new Date().getMinutes() + "</strong> " + this.owner.fighter.tempHistory + "</li>");
            } else {
                if (this.owner == player) {
                    alert("Vänta på att motståndaren spelat sin runda!")
                } else {
                    console.error("Opponent can't play a card because the canPaly var is false : " + this.owner.canPlay);
                }
            }
        }

    }
    playRandomCard() {
        console.log("Opponent will play a random card");
        this.owner.cardsOnHold.push(this.cards[Math.floor(Math.random() * this.cards.length)]);
        addToTimeline(100, game.nextTurn);
    }
    removeCard(card) {
        for (var c in this.cards) {
            if (this.cards[c] == card) {
                this.cards.splice(c, 1);
            }
        }
    }
    removeAllCards() {
        this.cards = [];
    }
    drawNewCard() {
        if (game == undefined || !game.ended) {
            var card = this.owner.deck.draw();
            if (card != undefined) {
                card.ownedBy(this);
                this.cards.push(card);
                if (card.type == "Attack") {
                    /* Elements */
                    if (this.owner.fighter.element == "eld" && this.owner.opponent.fighter.element == "jord") {
                        card.bonus = Math.round(card.value * 0.5);
                    } else if (this.owner.fighter.element == "jord" && this.owner.opponent.fighter.element == "luft") {
                        card.bonus = Math.round(card.value * 0.5);
                    } else if (this.owner.fighter.element == "luft" && this.owner.opponent.fighter.element == "vatten") {
                        card.bonus = Math.round(card.value * 0.5);
                    } else if (this.owner.fighter.element == "vatten" && this.owner.opponent.fighter.element == "eld") {
                        card.bonus = Math.round(card.value * 0.5);
                    }
                    console.log("Bonus: " + card.bonus);
                }
                console.log(this.owner);
                if (this.owner == player) {
                    console.log("Draw : " + card.id);
                    $(this.selector).append("<div></div>" + card.display("new") + "<div></div>");
                    console.log(card.display("new"));
                    var cardTemp = card.duplicate();
                    addToTimeline(500, function () {
                        $("body").append(cardTemp.display("drawn"));
                        $(cardTemp.selector).css({
                            "position": "fixed",
                            "left": window.innerWidth - 170 + "px",
                            "bottom": 0
                        });
                    });
                    cardTemp.moveToTarget(card);
                    addToTimeline(0, function () {
                        console.log(cardTemp.selector);
                        //card.hand.display();
                        $(cardTemp.selector).remove();
                        console.log("new");
                        $(card.selector).removeClass("new");
                    });
                }
            } else {
                console.error("drawNewCard : card undefined");
            }
        }
    }
    drawMissingCards() {
        var hand = this;
        var cardsToDraw = this.size - this.cards.length;
        if (this.owner == player) {
            console.log("DRAW MISSING");
            //this.display();
            $(hand.selector).addClass("drawing");
        }
        for (var i = 0; i < cardsToDraw; i++) {
            hand.drawNewCard();
        }
        if (this.owner == player) {
            addToTimeline(0, function () {
                console.log("---");
                $(hand.selector).removeClass("drawing");
                hand.refresh();
            });
        }
    }
    display() {
        var htmlOutput = "";
        if (this.owner == player || this.selector == "#hand") {
            for (var c in this.cards) {
                htmlOutput += "<div></div>";
                console.log();
                //if (game != undefined && game.turnsPlayed > 0 && c >= this.cards.length - 1 && $("#hand>.card").length != this.cards.length) {
                if (c >= this.owner.cardsPerTurn && $("#hand>.card").length != this.cards.length) {
                    htmlOutput += this.cards[c].display("new");
                } else {
                    htmlOutput += this.cards[c].display();
                }
                htmlOutput += "<div></div>";
            }
        } else {
            for (var c in this.cards) {
                htmlOutput += "<span class=\"card\"></span>";
            }
        }
        return (htmlOutput);
    }
    refresh() {
        var hand = this;
        if (this.owner == player || this.selector == "#hand") {
            this.log();
            console.log(this.cards);
            console.log("-----");
            $(hand.selector).html(this.display());
            $(".card").on("play", onPlayCard);
            $("#hand>.card:not(.played)").draggable({
                revert: "invalid",
                start: function (event, ui) {
                    $(this).children("button").remove();
                    $(this).removeClass("selected");
                    $("#biblomonBoard").css("background-color", "#f0f0f0");
                },
                stop: function (event, ui) {
                    if (!$(this).hasClass("hover-board")) {
                        $(this).css({
                            "top": "",
                            "height": "",
                            "width": "",
                            "transform": ""
                        });
                    }
                    $("#biblomonBoard").css("background-color", "");
                }
            }).click(function () {
                $(".card button.cancel, .card button.play").remove();
                $(".card").removeClass("selected");

                $(this).addClass("selected").append("<button class=\"cancel\">Cancel</button>").prepend("<button class=\"play\">Play card</button>");

                $("button.play").click(function () {
                    $(this).parents(".card").trigger("play");
                });
            });
            $("body").click(function (event) {
                if ($(event.target).hasClass("card") || $(event.target).parents().hasClass("card")) {
                    console.log("card");
                } else {
                    $(".card button.cancel, .card button.play").remove();
                    $(".card").removeClass("selected");
                }
            });
            $("#biblomonBoard" /*, #biblomon, #bokdrake"*/ ).droppable({
                hoverClass: "drop-hover",
                accept: ".card",
                classes: {
                    "ui-droppable-active": "ui-state-active",
                    "ui-droppable-hover": "ui-state-hover"
                },
                over: function (event, ui) {
                    $(ui.draggable).addClass("hover-board");
                },
                out: function (event, ui) {
                    $(ui.draggable).removeClass("hover-board");
                },
                drop: function (event, ui) {
                    if (!$(ui.draggable).hasClass("played")) {
                        $(ui.draggable).trigger("play");
                    }
                }
            });
        } else {
            $(hand.selector).html(this.display());
        }

    }
    log() {
        console.log("-- Hand log --");
        var htmlOutput = "";
        for (var c in this.cards) {
            htmlOutput += this.cards[c].log();
        }
        console.log("/- Hand log --");
    }
}